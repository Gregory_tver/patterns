<?php

namespace App\Domains\Order\Infrastructure\Models;

use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    protected $table = 'orders';
}
