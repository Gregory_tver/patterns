<?php

namespace App\Domains\Order\Infrastructure\Models;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $table = 'products';
}
