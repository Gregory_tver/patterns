<?php

namespace App\Domains\Order\Domain\Factories\Order;

use App\Domains\Order\Application\Requests\CreateOrderRequest;
use App\Domains\Order\Domain\Entity\Order\AbstractOrder;
use App\Domains\Order\Domain\Entity\Order\OrderFromPhoneCall;
use App\Domains\Order\Domain\ValueObjects\Address;
use App\Domains\Order\Domain\ValueObjects\EmployeeID;
use App\Domains\Order\Domain\ValueObjects\Phone;
use App\Domains\Order\Domain\ValueObjects\ShopID;

class OrderPhoneCallFactory implements OrderFactoryInterface
{
    public function makeOrder(CreateOrderRequest $request): AbstractOrder
    {
        return new OrderFromPhoneCall(
            new ShopID($request->shopId),
            new Phone($request->phone),
            new Address($request->deliveryAddress),
            new EmployeeID($request->employeeId),
        );
    }
}
